import { Component, ViewChild } from '@angular/core';
import { CurrencyService } from './currency.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  today = Date.now();
  message = '';
  @ViewChild('origin') origin;
  @ViewChild('destination') destination;
  @ViewChild('currencyOrigin') currencyOrigin;
  @ViewChild('currencyDestination') currencyDestination;
  currencies = [
    'USD', 'PEN', 'EUR' , 'JPY' ,'CNY'
  ]
  constructor(private convertService: CurrencyService) {}
  
  changeAmount() {
    this.message = '';
    let amount = this.origin.nativeElement.value;
    let origin = this.currencyOrigin.nativeElement.value;
    let destination = this.currencyDestination.nativeElement.value;

    if (amount && typeof +amount == 'number') {
      console.log('number', origin, destination);
      
      if (origin !== destination) {
        this.convertService.convert(origin, destination, amount)
          .then(data => this.destination.nativeElement.value = data.changeAmount)
          .catch(e => {
            this.destination.nativeElement.value = '----.--';
            this.message = 'Error al procesar';
          });
      } else {
        this.destination.nativeElement.value = amount;
      }
    }
    
  }
  invertCurrency() {
    let origin = this.currencyOrigin.nativeElement.value;
    let destination = this.currencyDestination.nativeElement.value;
    this.currencyOrigin.nativeElement.value = destination;
    this.currencyDestination.nativeElement.value = origin;
    console.log('invert');
    this.changeAmount();
  }
}
