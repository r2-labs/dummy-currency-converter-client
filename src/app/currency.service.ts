import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  constructor() { }

  convert(origin: string, destination: string, amount: number): Promise<any> {
    return fetch(`${environment.currencyService}/converter/from/${origin}/to/${destination}/amount/${amount}`)
      .then(r => r.json())
      .then(d => d[0])
  }
}
